/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JOptionPane;

/**
 *
 * @author Junior
 */
public class MenuProduto {
    
     public char menuProduto(){
        
        String menu = "MENU PRODUTO \n" + 
                      "1 - Cadastrar \n" +
                      "2 - Remover \n" +
                      "3 - Consultar \n" +
                      "4 - Listar \n" +
                      "0 - Retornar ao Menu Principal \n";
        
        return JOptionPane.showInputDialog(menu).charAt(0);
    } 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;


import javax.swing.JOptionPane;
import model.entities.Veiculo;

/**
 *
 * @author Junior
 */
public class MenuVeiculo {
    
    public char menuVeiculo(){
        
        String menu = "MENU VEICULO \n" + 
                      "1 - Cadastrar \n" +
                      "2 - Remover \n" +
                      "3 - Consultar \n" +
                      "4 - Listar \n" +
                      "0 - Retornar ao Menu Principal \n";
        
        return JOptionPane.showInputDialog(menu).charAt(0);
    }    
    
    public Veiculo cadastrar(){
        Veiculo veiculo = new Veiculo();
        String[] tipoVeiculo = {"Caminhao", "Carreta", "Van"};
        String opcao = (String) JOptionPane.showInputDialog(null, 
                                                "Escolha o tipo do veiculo", 
                                                "Veiculo",
                                                JOptionPane.INFORMATION_MESSAGE,
                                                null,
                                                tipoVeiculo, 
                                                tipoVeiculo[0]);      
       
       veiculo.setTipo(opcao);
       veiculo.setMarca(JOptionPane.showInputDialog("Marca: "));
       veiculo.setModelo(JOptionPane.showInputDialog("Modelo: "));
       veiculo.setPlaca(JOptionPane.showInputDialog("Placa (XXX-0000): "));
       veiculo.setAno(JOptionPane.showInputDialog("Ano: "));
       
       return veiculo;
    }
    
    public String remover(){
        return consultar();
    }
    
    public String consultar(){        
        return JOptionPane.showInputDialog("Placa: ");
    }
    
    public void listar(Veiculo veiculo){
        JOptionPane.showMessageDialog(null, veiculo);
    }
}

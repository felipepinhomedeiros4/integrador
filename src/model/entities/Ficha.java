
package model.entities;

public class Ficha {
    
    protected Integer id;
    protected Produto produto;
    protected Boolean situacao;
    protected Cliente cliente;

    public Ficha() {
    }

    public Ficha( int id, boolean situacao){
        this.id = id;
        this.situacao = situacao;
        this.produto = produto;
       }
        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSituacao() {
        return situacao;
    }

    public void setSituacao(Boolean situacao) {
        this.situacao = situacao;
    }
    
    
}

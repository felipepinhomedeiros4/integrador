package model.entities;

public class Atendente extends Pessoa {

    public Atendente() {
    }

    public Atendente(int id, String nome, String rua, String bairro, String cidade, String estado, String pais, int numero) {
        super(id, nome, rua, bairro, cidade, estado, pais, numero);
    }
}

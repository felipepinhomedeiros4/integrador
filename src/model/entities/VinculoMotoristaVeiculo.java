/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

/**
 *
 * @author Junior
 */
public class VinculoMotoristaVeiculo {
    
    private Integer id;
    private Motorista motorista;
    private Veiculo veiculo;
    private Integer quantidade;
    private Ficha ficha;

    public VinculoMotoristaVeiculo() {
    }

    public VinculoMotoristaVeiculo(Integer id, Motorista motorista, Veiculo veiculo, Integer quantidade, Ficha ficha) {
        this.id = id;
        this.motorista = motorista;
        this.veiculo = veiculo;
        this.quantidade = quantidade;
        this.ficha = ficha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Motorista getMotorista() {
        return motorista;
    }

    public void setMotorista(Motorista motorista) {
        this.motorista = motorista;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Ficha getFicha() {
        return ficha;
    }

    public void setFicha(Ficha ficha) {
        this.ficha = ficha;
    }
    
    
}

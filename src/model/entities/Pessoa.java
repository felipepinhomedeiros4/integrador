
package model.entities;


public class Pessoa extends Endereco{
    
    private int id;
    private String nome;    
    
    public Pessoa() {
    }

    public Pessoa(int id, String nome, String rua, String bairro, String cidade, String estado, String pais, int numero) {
        super(rua, bairro, cidade, estado, pais, numero);
        this.id = id;
        this.nome = nome;
    }
 
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }    
}
